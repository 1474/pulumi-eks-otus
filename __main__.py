from pulumi_kubernetes import Provider
import pulumi_eks as eks
import pulumi
import pulumi_awsx as awsx
import pulumi_aws as aws
import json
from pulumi import ResourceOptions, log
from pulumi_kubernetes.core.v1 import ServiceAccount
from pulumi_kubernetes.helm.v3 import Chart, ChartOpts, FetchOpts


config = pulumi.Config()
vpc_network_cidr = config.get("vpcNetworkCidr", "10.0.0.0/16")
node_public_key = config.get("node_public_key", "")
node_root_volume_size = "40Gi"
node_groups = config.get_object("node_groups", {})
aws_account_id = config.get("aws_account_id", "")
certs = {}
aws_config = pulumi.Config("aws")
aws_region = aws_config.get("region")
allowed_cidr_blocks = config.get("allowedCidrBlocks", [])
allowed_ipv6_cidr_blocks = config.get("allowedIPv6CidrBlocks", [])


instance_role = aws.iam.Role(
    "eksClusterRole",
    assume_role_policy=json.dumps({
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Principal": {
                    "Service": ["eks.amazonaws.com", "ec2.amazonaws.com"]
                },
                "Action": "sts:AssumeRole"
            }
        ]
    }),
    tags={
        "bill": "eks-poc",
    })

policyArns = [
    "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly",
    "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy",
    "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy",
    "arn:aws:iam::aws:policy/service-role/AmazonEBSCSIDriverPolicy",
    "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy",
    f"arn:aws:iam::{aws_account_id}:policy/Vector",
    f"arn:aws:iam::{aws_account_id}:policy/s3-ca-backend-loki-rw"
]


for idx, policyArn in enumerate(policyArns):
    aws.iam.RolePolicyAttachment(
        f"instance-role-policy-{idx}",
        policy_arn=policyArn,
        role=instance_role.name
    )


eks_vpc = awsx.ec2.Vpc("eks-vpc",
                       enable_dns_hostnames=True,
                       cidr_block=vpc_network_cidr,
                       subnet_specs=[
                           awsx.ec2.SubnetSpecArgs(
                               type=awsx.ec2.SubnetType.PRIVATE,
                               tags={
                                   "kubernetes.io/role/internal-elb": "1",
                               },
                           ),
                           awsx.ec2.SubnetSpecArgs(
                               type=awsx.ec2.SubnetType.PUBLIC,
                               tags={
                                   "kubernetes.io/role/elb": "1",
                               },
                           )
                       ])

allowed_cidr_blocks = pulumi.Config().require("allowedCidrBlocks")
allowed_ipv6_cidr_blocks = pulumi.Config().require("allowedIPv6CidrBlocks")

cluster_security_group = aws.ec2.SecurityGroup(
    "Cluster Access Group",
    vpc_id=eks_vpc.vpc_id,
    ingress=[aws.ec2.SecurityGroupIngressArgs(
        description="ALL FROM Whitelist",
        from_port=0,
        to_port=0,
        protocol=-1,
        cidr_blocks=allowed_cidr_blocks,
        ipv6_cidr_blocks=allowed_ipv6_cidr_blocks,
    )],
    egress=[aws.ec2.SecurityGroupEgressArgs(
        from_port=0,
        to_port=0,
        protocol="-1",
        cidr_blocks=["0.0.0.0/0"],
        ipv6_cidr_blocks=["::/0"],
    )],
    tags={
        "Name": "allow_tls",
    }
)


eks_cluster = eks.Cluster(
    "eks-cluster",
    vpc_id=eks_vpc.vpc_id,
    public_subnet_ids=eks_vpc.public_subnet_ids,
    private_subnet_ids=eks_vpc.private_subnet_ids,
    node_public_key=node_public_key,
    create_oidc_provider=True,
    node_associate_public_ip_address=False,
    instance_role=instance_role,
    cluster_security_group=cluster_security_group,
    skip_default_node_group=True
)

eks_provider = Provider("eks-k8s", kubeconfig=eks_cluster.kubeconfig_json)
oidc_provider = eks_cluster.core.oidc_provider

aws.eks.Addon(
    "ebs-csi-driver",
    addon_name="aws-ebs-csi-driver",
    addon_version="v1.24.1-eksbuild.1",
    cluster_name=eks_cluster.core.cluster.name,
    opts=ResourceOptions(depends_on=eks_cluster,
                         provider=eks_provider)
)

subnet_ids = pulumi.Output.all(
    eks_vpc.public_subnet_ids,
    eks_vpc.private_subnet_ids).apply(
    lambda ids: ids[0] + ids[1])

node_groups_deployed = {}

for node_group_name, node_group in node_groups.items():
    log.debug(f" ++++++++++++ Deploying node group {node_group_name}, settings: {node_group}")

    _node_group = aws.eks.NodeGroup(
        node_group_name,
        taints=node_group['taints'] if 'taints' in node_group else None,
        instance_types=[node_group['node_type']],
        cluster_name=eks_cluster.core.cluster.name,
        node_role_arn=instance_role.arn,
        subnet_ids=subnet_ids,
        scaling_config=aws.eks.NodeGroupScalingConfigArgs(
            desired_size=node_group['size']['desired'],
            max_size=node_group['size']['max'],
            min_size=node_group['size']['min'],
        ),
        update_config=aws.eks.NodeGroupUpdateConfigArgs(
            max_unavailable=1,
        ),
        opts=pulumi.ResourceOptions(
            depends_on=[eks_cluster], provider=eks_provider)
    )
    node_groups_deployed[node_group_name] = _node_group

# Get the OIDC provider URL
oidc_provider_url = eks_cluster.core.oidc_provider.url.apply(lambda url: url.split("/")[-1])  # noqa: E501
oidc_provider_url_str = oidc_provider_url.apply(lambda url: str(url))
# Define the trust policy
# Define the IAM roles and policy attachments
iam_roles = {
    "elb-role": {
        "trust_policy_sub": "system:serviceaccount:kube-system:aws-load-balancer-controller",  # noqa: E501
        "policy_arn": f"arn:aws:iam::{aws_account_id}:policy/AWSLoadBalancerControllerIAMPolicy"  # noqa: E501
    },
    "s3-ca-backend-loki-rw": {
        "trust_policy_sub": "system:serviceaccount:observability:loki",
        "policy_arn": f"arn:aws:iam::{aws_account_id}:policy/s3-ca-backend-loki-rw"  # noqa: E501
    },
    "s3-ca-backend-vector-ro": {
        "trust_policy_sub": "system:serviceaccount:observability:vector",
        "policy_arn": f"arn:aws:iam::{aws_account_id}:policy/s3-ca-backend-vector-ro"  # noqa: E501
    }
}

iam_roles_deployed = {}

# Create the IAM roles and policy attachments using loops
for role_name, iam_role in iam_roles.items():
    trust_policy = pulumi.Output.all(aws_account_id, oidc_provider_url_str, aws_region).apply(lambda args: {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Principal": {
                    "Federated": f"arn:aws:iam::{args[0]}:oidc-provider/oidc.eks.{args[2]}.amazonaws.com/id/{args[1]}"
                },
                "Action": "sts:AssumeRoleWithWebIdentity",
                "Condition": {
                    "StringEquals": {
                        f"oidc.eks.{args[2]}.amazonaws.com/id/{args[1]}:aud": "sts.amazonaws.com",
                        f"oidc.eks.{args[2]}.amazonaws.com/id/{args[1]}:sub": iam_role["trust_policy_sub"]
                    }
                }
            }
        ]
    })

    iam_role_obj = aws.iam.Role(
        role_name,
        assume_role_policy=pulumi.Output.secret(trust_policy),
        opts=pulumi.ResourceOptions(depends_on=[eks_cluster])
    )

    aws.iam.RolePolicyAttachment(
        f"{role_name}-policy",
        policy_arn=iam_role["policy_arn"],
        role=iam_role_obj.name,
        opts=pulumi.ResourceOptions(depends_on=[eks_cluster, iam_role_obj])
    )
    iam_roles_deployed[role_name] = iam_role_obj


# OPTIONAL FURTHER

# Create the ServiceAccount
role_arn = pulumi.Output.all(aws_account_id, "elb-role").apply(lambda args: f'arn:aws:iam::{args[0]}:role/{args[1]}')
service_account = ServiceAccount(
    'aws-load-balancer-controller',
    metadata={
        'labels': {
            'app.kubernetes.io/component': 'controller',
            'app.kubernetes.io/name': 'aws-load-balancer-controller',
        },
        'name': 'aws-load-balancer-controller',
        'namespace': 'kube-system',
        'annotations': {
            'eks.amazonaws.com/role-arn': role_arn,
        },
    },
    opts=pulumi.ResourceOptions(
        provider=eks_provider,
        depends_on=[iam_roles_deployed["elb-role"]]
    )
)

chart = Chart(
    'aws-load-balancer-controller',
    ChartOpts(
        chart="aws-load-balancer-controller",
        namespace="kube-system",
        fetch_opts=FetchOpts(
            repo="https://aws.github.io/eks-charts"
        ),
        values={
            "clusterName": eks_cluster.core.cluster.name,
            "serviceAccount": {
                "create": False,
                "name": "aws-load-balancer-controller"
            },
            "region": aws_region,
            "vpcId": eks_vpc.vpc_id,
        }
    ),
    opts=pulumi.ResourceOptions(
        provider=eks_provider,
        depends_on=[node_groups_deployed["infra"], service_account]
    )
)

pulumi.export("kubeconfig", eks_cluster.kubeconfig_json)
pulumi.export("service_account", service_account.metadata)
pulumi.export("IAM Cluster Role", instance_role.arn)
pulumi.export("Node_Groupd", node_groups_deployed)
