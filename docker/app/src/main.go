package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"path"
	"strconv"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	opsProcessed = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "myapp_processed_ops_total",
		Help: "The total number of processed events",
	})
)

func setMetric(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("got %s request\n", r.URL.Path)
	inc := path.Base(r.URL.Path)
	fmt.Println(inc)
	intVar, err := strconv.Atoi(inc)
	if err != nil {
		io.WriteString(w, fmt.Sprintf("Error: %s!\n", err))
	}
	opsProcessed.Set(float64(intVar))
	io.WriteString(w, "OK\n")
}

func main() {
	r := prometheus.NewRegistry()
	r.MustRegister(opsProcessed)
	handler := promhttp.HandlerFor(r, promhttp.HandlerOpts{})
	http.Handle("/metrics", handler)
	opsProcessed.Set(10)
	http.HandleFunc("/set/", setMetric)

	log.Fatal(http.ListenAndServe(":80", nil))
}
